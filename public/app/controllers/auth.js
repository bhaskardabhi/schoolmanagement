/// <reference path="angular.min.js" />

var myApp = angular.module('myModule',[]);

myApp.controller('AuthController',function($scope,$http,$window){
var loginError = false;

	$scope.save = function() {

        var data = {username: $scope.username, password: $scope.password};

	    $http({
            method: 'POST',
            url: API_URL + '/login',
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            if(response.data.success == true){ console.log(response.data.user);
                $window.location.href = URL + '/dashboard';
            }
            else{   
                $scope.loginError = true;
            }
        }).catch(function(response) {
            if(response.status == 422){ 
                $('#username_error').text(response.data['username']);
                $('#password_error').text(response.data['password']);
            }
        });

	}


});
