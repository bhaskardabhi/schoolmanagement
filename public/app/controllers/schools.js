/// <reference path="angular.min.js" />

var myApp = angular.module('myModule',[]);

myApp.controller('SchoolController',function($scope,$http,$window){
var loginError = false;

    $scope.signup = function() {

        var data = {name: $scope.name, domain: $scope.domain, username: $scope.username,email: $scope.email, phone: $scope.phone, password: $scope.password, password_confirmation: $scope.password_confirmation};
        
        $http({
            method: 'POST',
            url: API_URL + '/schools/signup',
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            if(response.status == 200){ 
                $window.location.href = URL + '/dashboard';
            }
            else{   
                 $scope.SignupError = true;               
            }
        }).catch(function(response) {
            if(response.status == 422){ alert('errror');
                $('#name_error').text(response.data['name']);
                $('#username_error').text(response.data['username']);
                $('#domain_error').text(response.data['domain']);
                $('#email_error').text(response.data['email']);
                $('#phone_error').text(response.data['phone']);
                $('#password_error').text(response.data['password']);
                $('#password_confirmation_error').text(response.data['password_confirmation']);
            }
        });

    }


});

