<?php

namespace App\Http\Requests\Api\School\Gallery\Image;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Models\Role;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "image" => "required|active_url",
        ];
    }
}
