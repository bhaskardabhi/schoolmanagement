<?php

namespace App\Http\Requests\Api\School\Invite;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Models\User;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required_without:user_id|string",
            "email" => "required_without:user_id|email",
            "phone" => "required_without:user_id|phone",
            "password" => "required_without:user_id|confirmed|min:6",
            "username" => "required_without:user_id|alpha_num|unique:".(new User)->getTable(),

            "user_id" => "integer|exists:".(new User)->getTable().",id|not_attached_school_user:".$this->route('id').",".$this->user_id,
        ];
    }
}
