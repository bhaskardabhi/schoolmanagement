<?php

namespace App\Http\Requests\Api\School\Invite;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Models\Role;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => "required|email",
            "name" => "required|string",
            "phone" => "required|phone",
            "role_id" => "required|integer|exists:".(new Role)->getTable().",id",
        ];
    }
}
