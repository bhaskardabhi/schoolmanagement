<?php

namespace App\Http\Requests\Api\School\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Repositories\School;

class DetachRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $schoolID = $this->route('school');
        $userID = $this->route('user');

        // Should be logged in user & role should be super admin only
        $auth = \Auth::user();

        return $auth and (new School)->isUserAttached($schoolID, $auth->id) and (new School)->isUserSuperAdmin($schoolID, $auth->id) and !(new School)->isUserSuperAdmin($schoolID, $userID);
    }


    public function rules()
    {
        return [];
    }
}
