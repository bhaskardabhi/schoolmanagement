<?php

namespace App\Http\Requests\Api\School;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;
use App\Models\School;
use Auth;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string",
            "username" => "required|alpha_num|unique:".(new User)->getTable(),
            "domain" => "required|alpha_num|unique:".(new School)->getTable(),
            "email" => "required|email",
            "phone" => "required|phone",
            "password" => "required|confirmed|min:6",
        ];
    }
}
