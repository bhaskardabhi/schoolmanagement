<?php

namespace App\Http\Validators;

class Base64
{
    public function validate($attribute, $formula, $validatorParameters, $validator)
    {
        //  Check if data:*/*;base64, exist;
        $separator = ";base64,";
        $position = strpos($formula, $separator);
        $base64String = $formula;

        if ($position !== false) {
            $base64String = substr($formula, $position + strlen($separator));
        }

        return base64_decode($base64String, true) !== false;
    }
}
