<?php

namespace App\Http\Validators;

class Phone
{
    public function validate($attribute, $formula, $validatorParameters, $validator)
    {
        //  Check if data:*/*;base64, exist;
        return strlen($formula) == 10;
    }
}
