<?php

namespace App\Http\Validators;
use App\Models\School;

class NotAttachedSchoolUser
{
    public function validate($attribute, $formula, $validatorParameters, $validator)
    {
        $schoolID  = $validatorParameters[0];
        $userID  = $validatorParameters[1];

        return School::findOrFail($schoolID)->users()->whereUserId($userID)->count() == 0;
    }
}
