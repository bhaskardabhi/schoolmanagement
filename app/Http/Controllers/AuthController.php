<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\School;
use Session;

class AuthController extends Controller
{
    public function Login()
    {
        return view('login');
    }


    public function PostLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        if(Auth::attempt(['username' => $request->username, 'password' => $request->password]))
        {
            return redirect('dashboard');
        }
        else
        {
            Session::flash('flash_message','Username or password is incorrect');
            return redirect('login');
        }
    }


    public function Logout()
    {
        Auth::logout();
        return redirect('login');
    }


    public function dashboard()
    {
    	return view('user.dashboard');
    }


    public function LogedInUserInfo()
    {
        $data['user'] = Auth::user();
        $data['schools'] = School::where('added_by',Auth::user()->id)->first();

        return response()->json($data);
    }

}
