<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Role;

class RoleController extends Controller
{
    public function index()
    {
        return response()->json(
            (new Role)->all()
        );
    }

    public function show($id)
    {
        return response()->json((new Role)->findOrFail($id));
    }
}
