<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\TempImage\CreateRequest;
use App\Repositories\TempImage;
use Carbon\Carbon;
use Image;

class TempImageController extends Controller
{
    public function store(CreateRequest $request)
    {
        if($image = $this->saveFile($request->image)){
            return response()->json(
                (new TempImage)->add([
                    'image' => $image,
                    'added_by' => \Auth::user()->id
                ])
            );
        } else {
            return response()->apiError([
                'type' => 'internal_error',
                'message' => 'Error Creating Temp Image',
            ]);
        }
    }

    private function saveFile($base64)
    {
        $extension = explode('/', explode(';', $base64)[0])[1];

        $image = Image::make($base64)->save("images/temp/".Carbon::now()->format('d_m_Y_h_i_s')."_".str_random(40). '.' . $extension);

        if($image){
            return asset($image->basePath());
        } else {
            return false;
        }
    }
}
