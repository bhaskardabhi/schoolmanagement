<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User;

class SchoolController extends Controller
{
    public function index($userID)
    {
        if($userID == "me"){
            $userID = \Auth::user()->id;
        }

        return response()->json(
            (new User)->getSchools($userID)
        );
    }
}
