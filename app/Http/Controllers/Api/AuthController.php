<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Repositories\User;
use Auth;

class AuthController extends Controller
{
    public function doLogin(LoginRequest $request)
    {
        Auth::attempt(['username' => $request->username, 'password' => $request->password]);

        return response()->json([
            'user' => Auth::user()
        ]);
    }

    public function doLogout()
    {
        return response()->json([
            'success' => Auth::logout()
        ]);
    }
}
