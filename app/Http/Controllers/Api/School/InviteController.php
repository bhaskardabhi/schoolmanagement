<?php

namespace App\Http\Controllers\Api\School;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\School\Invite\CreateRequest;
use App\Http\Requests\Api\School\Invite\UpdateRequest;
use App\Http\Requests\Api\School\Invite\SignUpRequest;
use App\Repositories\School;
use App\Repositories\Invite;
use App\Repositories\User;

class InviteController extends Controller
{
    public function index(Request $request, $schoolID)
    {
        $where = [];
        foreach (collect($request->get('where',[])) as $key => $value) {
            $whereParam = explode('=',$value);

            $where[$whereParam[0]] = $whereParam[1];
        }

        return response()->json(
            (new School)->where($where)->getPaginatedInvites($schoolID)
        );
    }

    public function store(CreateRequest $request, $schoolID)
    {
        $invite = (new Invite)->add([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'role_id' => $request->role_id,
            'school_id' => $schoolID,
            'added_by' => \Auth::user()->id,
        ]);

        if(!$invite){
            return response()->apiError([
                'type' => 'internal_error',
                'message' => 'Error Creating Invite',
            ]);
        } else {
            return response()->json($invite);
        }
    }

    public function show($schoolID, $slug)
    {
        return response()->json(
            (new Invite)->findByToken($slug)
        );
    }

    public function update(UpdateRequest $request, $schoolID, $slug)
    {
        $invite = (new Invite)->findByToken($slug);

        return response()->json([
            'success' => (new Invite)->update($invite->id, [
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'role_id' => $request->role_id,
                'school_id' => $schoolID,
            ])
        ]);
    }

    public function destroy($schoolID, $id)
    {
        return response()->json([
            'success' => (new Invite)->delete($id)
        ]);
    }

    public function doSignup(SignUpRequest $request, $schoolID, $token)
    {
        $userID = $request->user_id;

        if(!$userID){
            $user = (new User)->add($request->all());

            $userID = $user ? $user->id : null;
        }

        if($userID){
            return response()->json(
                (new Invite)->attachUserToInvite((new Invite)->findByToken($token)->id,$userID)
            );
        } else {
            return response()->apiError([
                'type' => 'internal_error',
                'message' => 'Error while Signup for Invite',
            ]);
        }
    }
}
