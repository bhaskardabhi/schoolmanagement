<?php

namespace App\Http\Controllers\Api\School;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\School\Gallery\Image\CreateRequest;
use App\Http\Requests\Api\School\Gallery\Image\UpdateRequest;
use App\Repositories\School;
use App\Repositories\GalleryImage;
use App\Repositories\Gallery;

class GalleryImageController extends Controller
{
    public function index($schoolID, $id)
    {
        return response()->json(
            (new Gallery)->getImages($id)
        );
    }

    public function store(CreateRequest $request, $schoolID, $id)
    {
        $invite = (new GalleryImage)->add([
            'image' => $request->image,
            'added_by' => \Auth::user()->id,
            'gallery_id' => $id,
        ]);

        if(!$invite){
            return response()->apiError([
                'type' => 'internal_error',
                'message' => 'Error Creating Gallery Image',
            ]);
        } else {
            return response()->json($invite);
        }
    }

    public function show($schoolID, $galleryId, $id)
    {
        return response()->json(
            (new GalleryImage)->findOrFail($id)
        );
    }

    public function update(UpdateRequest $request, $schoolID, $galleryId, $id)
    {
        return response()->json([
            'success' => (new GalleryImage)->update($id, [
                'image' => $request->image,
                'gallery_id' => $galleryId,
            ])
        ]);
    }

    public function destroy($schoolID, $galleryId, $id)
    {
        return response()->json([
            'success' => (new GalleryImage)->delete($id)
        ]);
    }
}
