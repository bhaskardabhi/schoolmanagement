<?php

namespace App\Http\Controllers\Api\School;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\School\SignUpRequest;
use App\Repositories\User;

class SignUpController extends Controller
{
    public function store(SignUpRequest $request)
    {
        return response()->json([
            'school' => (new User)->addUserWithSchool([
                'user' => [
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => $request->password,
                    'phone' => $request->phone,
                    'username' => $request->username,
                ],
                'school' => [
                    'name' => $request->name,
                    'domain' => $request->domain,
                    'email' => $request->email,
                    'phone' => $request->phone,
                ]
            ])
        ]);
    }
}
