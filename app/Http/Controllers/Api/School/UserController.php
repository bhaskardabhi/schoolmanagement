<?php

namespace App\Http\Controllers\Api\School;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\School;
use App\Http\Requests\Api\School\User\DetachRequest;

class UserController extends Controller
{
    public function index(Request $request,$schoolID)
    {
        return response()->json(
            (new School)->with($request->with ? explode(',', $request->with) : [])->getPaginatedUsers($schoolID)
        );
    }

    public function destroy(DetachRequest $request,$schoolID,$userID)
    {
        return response()->json([
            'success' => (new School)->detachUser($schoolID,$userID)
        ]);
    }
}
