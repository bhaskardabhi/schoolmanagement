<?php

namespace App\Http\Controllers\Api\School;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\School\Feedback\CreateRequest;
use App\Http\Requests\Api\School\Feedback\UpdateRequest;
use App\Repositories\School;
use App\Repositories\Feedback;

class FeedbackController extends Controller
{
    public function index(Request $request, $schoolID)
    {
        return response()->json(
            (new School)->with($request->with ? explode(',', $request->with) : [])->getPaginatedFeedbacks($schoolID)
        );
    }

    public function store(CreateRequest $request, $schoolID)
    {
        $invite = (new Feedback)->add([
            'feedback' => $request->feedback,
            'school_id' => $schoolID,
            'added_by' => \Auth::user()->id,
        ]);

        if(!$invite){
            return response()->apiError([
                'type' => 'internal_error',
                'message' => 'Error Creating Feedback',
            ]);
        } else {
            return response()->json($invite);
        }
    }

    public function show($schoolID, $id)
    {
        return response()->json((new Feedback)->findOrFail($id));
    }

    public function update(UpdateRequest $request, $schoolID, $id)
    {
        return response()->json([
            'success' => (new Feedback)->update($id, [
                'feedback' => $request->feedback,
                'school_id' => $schoolID,
            ])
        ]);
    }

    public function destroy($schoolID, $id)
    {
        return response()->json([
            'success' => (new Feedback)->delete($id)
        ]);
    }
}
