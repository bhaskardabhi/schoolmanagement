<?php

namespace App\Http\Controllers\Api\School;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\School\News\CreateRequest;
use App\Http\Requests\Api\School\News\UpdateRequest;
use App\Repositories\School;
use App\Repositories\News;

class NewsController extends Controller
{
    public function index($schoolID)
    {
        return response()->json(
            (new School)->getPaginatedNews($schoolID)
        );
    }

    public function store(CreateRequest $request, $schoolID)
    {
        $invite = (new News)->add([
            'title' => $request->title,
            'description' => $request->description,
            'added_by' => \Auth::user()->id,
            'school_id' => $schoolID,
        ]);

        if(!$invite){
            return response()->apiError([
                'type' => 'internal_error',
                'message' => 'Error Creating News',
            ]);
        } else {
            return response()->json($invite);
        }
    }

    public function show($schoolID, $id)
    {
        return response()->json((new News)->findOrFail($id));
    }

    public function update(UpdateRequest $request, $schoolID, $id)
    {
        return response()->json([
            'success' => (new News)->update($id, [
                'title' => $request->title,
                'description' => $request->description,
                'school_id' => $schoolID,
            ])
        ]);
    }

    public function destroy($schoolID, $id)
    {
        return response()->json([
            'success' => (new News)->delete($id)
        ]);
    }
}
