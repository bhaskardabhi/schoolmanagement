<?php

namespace App\Http\Controllers\Api\School;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\School\Gallery\CreateRequest;
use App\Http\Requests\Api\School\Gallery\UpdateRequest;
use App\Repositories\School;
use App\Repositories\Gallery;

class GalleryController extends Controller
{
    public function index($schoolID)
    {
        return response()->json(
            (new School)->getPaginatedGalleries($schoolID)
        );
    }

    public function store(CreateRequest $request, $schoolID)
    {
        $invite = (new Gallery)->add([
            'name' => $request->name,
            'description' => $request->description,
            'added_by' => \Auth::user()->id,
            'school_id' => $schoolID,
        ]);

        if(!$invite){
            return response()->apiError([
                'type' => 'internal_error',
                'message' => 'Error Creating Gallery',
            ]);
        } else {
            return response()->json($invite);
        }
    }

    public function show(Request $request, $schoolID, $id)
    {
        return response()->json(
            (new Gallery)->with($request->with ? explode(',', $request->with) : [])->findOrFail($id)
        );
    }

    public function update(UpdateRequest $request, $schoolID, $id)
    {
        return response()->json([
            'success' => (new Gallery)->update($id, [
                'name' => $request->name,
                'description' => $request->description,
                'school_id' => $schoolID,
            ])
        ]);
    }

    public function destroy($schoolID, $id)
    {
        return response()->json([
            'success' => (new Gallery)->delete($id)
        ]);
    }
}
