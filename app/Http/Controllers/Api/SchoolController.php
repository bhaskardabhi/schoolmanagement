<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\School;

class SchoolController extends Controller
{
    public function show($id)
    {
        return response()->json(
            (new School)->findOrFail($id)
        );
    }
}
