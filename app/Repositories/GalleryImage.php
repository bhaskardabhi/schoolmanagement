<?php

namespace App\Repositories;

use App\Models\GalleryImage as GalleryImageModel;

class GalleryImage extends Repository {

    public function model()
    {
        return new GalleryImageModel;
    }

    public function getImages(int $id)
    {
        return $this->model()->with(['images'])->findOrFail($id)->images;
    }
}
