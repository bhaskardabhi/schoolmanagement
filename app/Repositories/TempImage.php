<?php

namespace App\Repositories;

use App\Models\TempImage as TempImageModel;

class TempImage extends Repository {

    public function model()
    {
        return new TempImageModel;
    }

    public function add(array $data)
    {
        $model = $this->model()->fill($data);
        $model->added_by = $data['added_by'];

        return $model->save() ? $model : false;
    }
}
