<?php

namespace App\Repositories;

abstract class Repository {

    public $where = [];
    public $with = [];

    public function getProcessedModel()
    {
        return $this->model()->with($this->with)->where(function ($query) {
            return $this->applyWhere($query);
        });
    }

    abstract public function model();

    public function add(array $data)
    {
        $model = $this->model()->fill($data);

        return $model->save() ? $model : false;
    }

    public function update(int $id, array $data)
    {
        $model = $this->model()->findOrFail($id)->fill($data);

        return $model->save();
    }

    public function findOrFail(int $id)
    {
        return $this->model()->with($this->with)->findOrFail($id);
    }

    public function delete(int $id)
    {
        return $this->model()->findOrFail($id)->delete();
    }

    public function all()
    {
        return $this->model()->all();
    }

    public function where($where = [])
    {
        $this->where = $where;

        return $this;
    }

    public function with($with = [])
    {
        $this->with = $with;

        return $this;
    }

    public function getPaginated()
    {
        return $this->getProcessedModel()->paginate();
    }

    public function applyWhere($query)
    {
        foreach ($this->where as $column => $value) {
            $query->where($column,$value);
        }
    }
}
