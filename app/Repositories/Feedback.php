<?php

namespace App\Repositories;

use App\Models\Feedback as FeedbackModel;

class Feedback extends Repository {

    public function model()
    {
        return new FeedbackModel;
    }

    public function add(array $data)
    {
        $invite = $this->model()->fill($data);
        $invite->school_id = $data['school_id'];
        $invite->added_by = $data['added_by'];

        return $invite->save() ? $invite : false;
    }

    public function update(int $id, array $data)
    {
        $invite = $this->model()->findOrFail($id)->fill($data);
        $invite->school_id = $data['school_id'];

        return $invite->save();
    }
}
