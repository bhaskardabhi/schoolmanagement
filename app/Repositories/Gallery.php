<?php

namespace App\Repositories;

use App\Models\Gallery as GalleryModel;

class Gallery extends Repository {

    public function model()
    {
        return new GalleryModel;
    }

    public function add(array $data)
    {
        $invite = $this->model()->fill($data);
        $invite->school_id = $data['school_id'];

        return $invite->save() ? $invite : false;
    }

    public function update(int $id, array $data)
    {
        $invite = $this->model()->findOrFail($id)->fill($data);
        $invite->school_id = $data['school_id'];

        return $invite->save();
    }

    public function getImages(int $id)
    {
        return $this->model()->with(['images'])->findOrFail($id)->images;
    }
}
