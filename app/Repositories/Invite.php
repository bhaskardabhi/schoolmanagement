<?php

namespace App\Repositories;

use App\Models\Invite as InviteModel;

class Invite extends Repository {

    public function model()
    {
        return new InviteModel;
    }

    public function add(array $data)
    {
        $invite = $this->model()->fill($data);
        $invite->school_id = $data['school_id'];
        $invite->role_id = $data['role_id'];
        $invite->added_by = $data['added_by'];

        return $invite->save() ? $invite : false;
    }

    public function findByToken($token)
    {
        return $this->model()->whereToken($token)->latest()->first();
    }

    public function update(int $id, array $data)
    {
        $invite = $this->model()->findOrFail($id)->fill($data);
        $invite->school_id = $data['school_id'];
        $invite->role_id = $data['role_id'];

        return $invite->save();
    }

    public function attachUserToInvite(int $id, int $userID)
    {
        $invite = $this->model()->findOrFail($id);
        $invite->user_id = $userID;

        if(!$invite->save()){
            return false;
        }

        return (new School)->attachUser($invite->school_id, $userID, $invite->role_id);
    }
}
