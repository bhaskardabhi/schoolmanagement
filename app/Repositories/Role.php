<?php

namespace App\Repositories;

use App\Models\Role as RoleModel;

class Role extends Repository {

    public function model()
    {
        return new RoleModel;
    }
}
