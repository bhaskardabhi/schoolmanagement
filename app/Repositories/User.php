<?php

namespace App\Repositories;

use App\Models\School;
use App\Models\User as UserModel;
use App\Models\Role;

class User extends Repository{

    public function model()
    {
        return new UserModel;
    }

    public function addUserWithSchool(array $data)
    {
        return \DB::transaction(function() use ($data)
        {
            $user = $this->add($data['user']);

            if(!$user){
                throw new \Exception("Error Adding User");
            }

            return $this->addSchool(
                $user->id,
                (new School)->fill($data['school'])
            );
        });
    }

    public function addSchool($id, School $school)
    {
        $school->added_by = $id;

        if(!$school->save()){
            return false;
        }

        $school->users()->save($this->model()->findOrFail($id),[
            'role_id' => (new Role)->getSuperAdminRole()->id
        ]);

        $school->load("users");

        return $school;
    }

    public function getSchools($id)
    {
        return $this->model()->with(['schools'])->findOrFail($id)->schools;
    }

    public function add(array $data)
    {
        $user = $this->model()->fill($data);
        $user->username = $data['username'];

        return $user->save() ? $user : false;
    }

    public function applyWhere($query)
    {
        foreach ($this->where as $column => $value) {
            if($column == "school_id"){
                $query->whereHas('schools', function($query) use ($value)
                {
                    $query->whereSchoolId($value);
                });
            } else {
                $query->where($column,$value);
            }
        }
    }
}
