<?php

namespace App\Repositories;

use App\Models\School as SchoolModel;
use App\Repositories\User;

class School extends Repository {

    public function model()
    {
        return new SchoolModel;
    }

    public function getInvites($id)
    {
        return $this->model()->with(['invites.role'])->findOrFail($id)->invites;
    }

    public function getNews(int $id)
    {
        return $this->model()->with(['news'])->findOrFail($id)->news;
    }

    public function getFeedbacks(int $id)
    {
        return $this->model()->with(['feedbacks.user'])->findOrFail($id)->feedbacks;
    }

    public function getGalleries(int $id)
    {
        return $this->model()->with(['galleries'])->findOrFail($id)->galleries;
    }

    public function getAttachedUser(int $id, int $userID)
    {
        return $this->model()->findOrFail($id)->users()->whereUserId($userID)->first();
    }

    public function isUserAttached(int $id, int $userID)
    {
        return !!$this->getAttachedUser($id, $userID);
    }

    public function isUserSuperAdmin(int $id, int $userID)
    {
        $user = $this->getAttachedUser($id, $userID);

        if(!$user){
            return false;
        }

        return (new Role)->findOrFail($user->pivot->role_id)->slug == "super-admin";
    }

    public function attachUser(int $schoolID, int $userID, int $roleID)
    {
        return $this->model()->findOrFail($schoolID)->users()->save(
            (new User)->findOrFail($userID),
            ['role_id' => $roleID]
        );
    }

    public function detachUser(int $schoolID, int $userID)
    {
        return $this->findOrFail($schoolID)->users()->detach($userID);
    }

    public function getPaginatedNews(int $schoolID)
    {
        return (new News)->with($this->with)->where(array_merge(['school_id' => $schoolID],$this->where))->getPaginated();
    }

    public function getPaginatedInvites(int $schoolID)
    {
        return (new Invite)->with($this->with)->where(array_merge(['school_id' => $schoolID],$this->where))->getPaginated();
    }

    public function getPaginatedFeedbacks(int $schoolID)
    {
        return (new Feedback)->with($this->with)->where(array_merge(['school_id' => $schoolID],$this->where))->getPaginated();
    }

    public function getPaginatedGalleries(int $schoolID)
    {
        return (new Gallery)->with($this->with)->where(array_merge(['school_id' => $schoolID],$this->where))->getPaginated();
    }

    public function getPaginatedUsers(int $schoolID)
    {
        $model = $this->findOrFail($schoolID);

        return $model->users()->paginate();
    }
}
