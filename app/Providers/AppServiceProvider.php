<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator,Response,Route;
use App\Models\School;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Registering validatators
        Validator::extend('phone', 'App\Http\Validators\Phone@validate');
        Validator::extend('base64', 'App\Http\Validators\Base64@validate');
        Validator::extend('not_attached_school_user', 'App\Http\Validators\NotAttachedSchoolUser@validate');

        Response::macro('apiError', function ($error, $code = 500, $data = []) {
            return response()->make([
                'type' => isset($error['type']) ? $error['type'] : "",
                'message' => isset($error['message']) ? $error['message'] : "",
                'data' => $data,
            ], $code);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
