<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password','phone', 'domain'
    ];

    protected $dates = ['deleted_at'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'school_user_role', 'school_id', 'user_id')
                    ->withPivot('role_id');
    }

	public function invites()
	{
		return $this->hasMany(Invite::class);
	}

	public function news()
	{
		return $this->hasMany(News::class);
	}

	public function feedbacks()
	{
		return $this->hasMany(Feedback::class);
	}

	public function galleries()
	{
		return $this->hasMany(Gallery::class);
	}
}
