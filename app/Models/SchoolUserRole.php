<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolUserRole extends Model
{
	use SoftDeletes;

	protected $table = "school_user_role";
	
    protected $fillable = [
        'school_id', 'role_id', 'user_id'
    ];

    protected $dates = ['deleted_at'];

}
