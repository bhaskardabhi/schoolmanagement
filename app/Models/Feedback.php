<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'feedback','added_by','school_id'
    ];

    protected $dates = ['deleted_at'];

	public function user()
	{
		return $this->belongsTo(User::class,'added_by');
	}
}
