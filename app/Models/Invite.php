<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    protected $fillable = [
        'name', 'email', 'phone'
    ];

    protected $hidden = [
        // 'token' //TODO: WHy tokrn is hidden here. we need token to get signle invite
    ];

    public function setSchoolIdAttribute($value)
    {
        $this->attributes['school_id'] = $value;

        if(!isset($this->attributes['id']) || !$this->attributes['id']){
            $this->attributes['token'] = str_random(10);
        }
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function getSuperAdminRole()
    {
        return $this->whereSlug("super-admin")->first();
    }
}
