<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GalleryImage extends Model
{
	use SoftDeletes;
	
    protected $fillable = [
        'image','gallery_id','added_by'
    ];

    protected $dates = ['deleted_at'];

}
	