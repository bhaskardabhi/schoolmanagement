<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{

	use SoftDeletes;

    protected $fillable = [
        'slug', 'name'
    ];

    protected $dates = ['deleted_at'];

    public function getSuperAdminRole()
    {
        return $this->whereSlug("super-admin")->first();
    }
}
