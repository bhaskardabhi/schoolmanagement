<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name','description','added_by','school_id'
    ];

    protected $dates = ['deleted_at'];

	public function images()
	{
		return $this->hasMany(GalleryImage::class);
	}
}
