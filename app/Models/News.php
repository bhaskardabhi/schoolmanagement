<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'title','description','added_by','school_id'
    ];

    protected $dates = ['deleted_at'];
}
