@extends('layouts.app')

@section('content')


<div class="login-box">

  <div class="col-md-12">
     @if(Session::has('flash_message'))
            <div class="alert alert-danger">
                 <span class="glyphicon glyphicon-close"></span>
                    <em> {!! session('flash_message') !!}</em>
            </div>
        @endif
  </div>

  <div class="login-logo">
    <a href=""><b>SIGN IN</b></a>
  </div>
  <div class="login-box-body">

    {{ Form::open(array('method' => 'post','url' => url('login'), 'class' => "")) }}
      <div class="form-group has-feedback">
         {{ Form::text('username', null , $attributes= ['class' => 'form-control','placeholder' => 'Username']) }}
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @if($errors->first('username'))
              <div class="alert alert-danger">
                {{ $errors->first('username') }}
              </div>
            @endif
      </div>

      <div class="form-group has-feedback">
         {{ Form::password('password', ['class' => 'form-control','placeholder' => 'Password']) }}
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->first('password'))
              <div class="alert alert-danger">
                {{ $errors->first('password') }}
              </div>
            @endif
      </div>

      <div class="row">
        <div class="col-xs-12">
            {{ Form::submit('Sign In',[ "class" => "btn btn-primary btn-block btn-flat" ]) }}
        </div>
      </div>
    {{ Form::close() }}

  </div>
</div>

@endsection