@extends('layouts.app')

@section('content')

<div class="login-box">
		<div class="login-logo">
		    <a href=""><b>SIGN UP</b></a>
		</div>
  		

  		<div class="login-box-body" ng-controller="SchoolController">
			<form name="signupForm" method="post" ng-submit ="signup()">
			      
			    <p class="validation-error"  data-ng-messages="signupForm.$invalid" data-ng-show="SignupError" > An error occure while signup. </p>

			    <div class="form-group has-feedback" ng-class="{true: 'error'}[submitted && signupForm.name.$invalid]">
			        <input type="text" class="form-control input-feild" placeholder="School name" name="name" ng-model="name" ng-required="true">
			        <span class="glyphicon glyphicon-user form-control-feedback"></span>
			        <p class="validation-error" ng-show="submitted && signupForm.name.$error.required">School name feild is required.</p>
			        <p class="validation-error" id="name_error" ng-show="signupForm.name.$error"></p>
			    </div>

			    <div class="form-group has-feedback" ng-class="{true: 'error'}[submitted && signupForm.domain.$invalid]">
			        <input type="text" class="form-control input-feild" placeholder="Domain name" name="domain" ng-model="domain" ng-required="true">
			        <span class="glyphicon glyphicon-user form-control-feedback"></span>
			        <p class="validation-error" ng-show="submitted && signupForm.domain.$error.required">Domain name feild is required.</p>
			        <p class="validation-error" id="domain_error" ng-show="signupForm.domain.$error"></p>
			    </div>

			    <div class="form-group has-feedback" ng-class="{true: 'error'}[submitted && signupForm.email.$invalid]">
			        <input type="email" class="form-control input-feild" placeholder="Email" name="email" ng-model="email" ng-required="true">
			        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			        <p class="validation-error" ng-show="submitted && signupForm.email.$error.required">Email feild is required.</p>
			        <p class="validation-error" id="email_error" ng-show="signupForm.email.$error"></p>
			    </div>

			    <div class="form-group has-feedback" ng-class="{true: 'error'}[submitted && signupForm.phone.$invalid]">
			        <input type="text" class="form-control input-feild" placeholder="Phone" name="phone" ng-model="phone" ng-required="true">
			        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
			        <p class="validation-error" ng-show="submitted && signupForm.phone.$error.required">Phone feild is required.</p>
			        <p class="validation-error" id="phone_error" ng-show="signupForm.phone.$error"></p>
			    </div>

			    <div class="form-group has-feedback" ng-class="{true: 'error'}[submitted && signupForm.username.$invalid]">
			        <input type="text" class="form-control input-feild" placeholder="Username" name="username" ng-model="username" ng-required="true">
			        <span class="glyphicon glyphicon-user form-control-feedback"></span>
			        <p class="validation-error" ng-show="submitted && signupForm.username.$error.required">Username feild is required.</p>
			        <p class="validation-error" id="username_error" ng-show="signupForm.username.$error"></p>
			    </div>

			    <div class="form-group has-feedback" ng-class="{true: 'error'}[submitted && signupForm.password.$invalid]">
			        <input type="password" class="form-control" placeholder="Password" name="password" ng-model="password"  ng-required="true">
			        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
			        <p class="validation-error" ng-show="submitted && signupForm.password.$error.required">Password feild is required.</p>
			        <p class="validation-error" id="password_error" ng-show="signupForm.password.$error"></p>
			    </div>

			    <div class="form-group has-feedback" ng-class="{true: 'error'}[submitted && signupForm.password_confirmation.$invalid]">
			        <input type="password" class="form-control" placeholder="Re-type Password" name="password_confirmation " ng-model="password_confirmation"  ng-required="true">
			        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
			        <p class="validation-error" ng-show="submitted && signupForm.password_confirmation.$error.required">Re-type Password feild is required.</p>
			        <p class="validation-error" id="password_confirmation_error" ng-show="signupForm.password_confirmation.$error"></p>
			    </div>

		        <div class="row">
		            <div class="col-xs-12">
		              <button type="submit" class="btn btn-primary btn-block btn-flat" ng-click="submitted=true">Sign UP</button>
		            </div>
		        </div>
			</form>

		</div>
</div>
	
@endsection

