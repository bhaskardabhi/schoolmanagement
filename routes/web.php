<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return File::get(public_path('dist/views/index.html'));
});

Route::get("login","AuthController@Login");
Route::post("login","AuthController@PostLogin");
Route::get("logout","AuthController@Logout");

Route::get("user-info","AuthController@LogedInUserInfo");

Route::get("dashboard","AuthController@dashboard")->middleware('auth');

// Route::get('/schools/signup', 'SchoolController@signup');
