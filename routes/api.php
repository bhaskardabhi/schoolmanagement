<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api','prefix' => 'v1'], function ()
{
    Route::post("login","AuthController@doLogin");
    Route::post("schools/signup","School\SignUpController@store");

    Route::group(['middleware' => 'auth:api'], function (){
        Route::get("users/me","UserController@me");
        Route::get("logout","AuthController@doLogout")->name('api.logout');
        Route::resource("temp-images",'TempImageController',['only' => ['store']]);

        Route::resource("roles",'RoleController',['only' => ['index','show']]);
        Route::resource("schools",'SchoolController', ['only' => ['show']]);

        Route::group(['namespace' => 'User'], function (){
            Route::resource("users.schools",'SchoolController');
        });

        Route::group(['namespace' => 'School'], function (){
            Route::post("schools/{id}/invites/{token}/signup","InviteController@doSignup");

            Route::resource("schools.invites",'InviteController');
            Route::resource("schools.users",'UserController');
            Route::resource("schools.news",'NewsController');
            Route::resource("schools.feedbacks",'FeedbackController');
            Route::resource("schools.galleries",'GalleryController');
            Route::resource("schools.galleries.images",'GalleryImageController');
        });
    });

});
