<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Role;

class AddSuperadminRoleInRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            'slug' => 'super-admin',
            'name' => 'Super Admin'
        ];

        if(!(new Role)->fill($data)->save()){
            throw new \Exception("Error Creating Super admin role");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!(new Role)->getSuperAdminRole()->delete()){
            throw new \Exception("Error Deleting Super admin role");
        }
    }
}
