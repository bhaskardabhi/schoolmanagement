var api_token;
var underscore = angular.module('underscore', []);
underscore.factory('_', ['$window', function($window) {
  return $window._; // assumes underscore has already been loaded on the page
}]);
var app = angular.module('project', ['ngRoute','ngCookies','underscore','ui.bootstrap','ui.bootstrap.tpls'])
.config(function($routeProvider) {
  $routeProvider
    .when('/login', {
      controller:'LoginController',
      templateUrl:'/dist/views/login.html',
      data: {
          requiredAuth: false
      }
    })
    .when('/logout', {
      controller:'LogoutController',
      data: {
          requiredAuth: false
      }
    })
    .when('/schools/signup', {
      controller:'SchoolSignupController',
      templateUrl:'/dist/views/schools/signup.html',
      data: {
          requiredAuth: false
      }
    })
    .when('/schools/:schoolID', {
      controller:'SchoolDashboardController',
      templateUrl:'/dist/views/schools/dashboard.html',
      data: {
          requiredAuth: false
      }
    })
    .when('/schools/:schoolID/invites', {
      controller:'InviteListController',
      templateUrl:'/dist/views/schools/invites/all.html'
    })
    .when('/schools/:schoolID/invites/create', {
      controller:'InviteCreateController',
      templateUrl:'/dist/views/schools/invites/create.html'
    })
    .when('/schools/:schoolID/invites/:token/edit', {
      controller:'InviteEditController',
      templateUrl:'/dist/views/schools/invites/edit.html'
    })
    .when('/schools/:schoolID/news/create', {
      controller:'NewsCreateController',
      templateUrl:'/dist/views/schools/news/create.html'
    })
    .when('/schools/:schoolID/news', {
      controller:'NewsListController',
      templateUrl:'/dist/views/schools/news/all.html'
    })
    .when('/schools/:schoolID/news/:id/edit', {
      controller:'NewsEditController',
      templateUrl:'/dist/views/schools/news/edit.html'
    })
    .when('/schools/:schoolID/feedbacks/create', {
      controller:'FeedbackCreateController',
      templateUrl:'/dist/views/schools/feedbacks/create.html'
    })
    .when('/schools/:schoolID/feedbacks', {
      controller:'FeedbackListController',
      templateUrl:'/dist/views/schools/feedbacks/all.html'
    })
    .when('/schools/:schoolID/users', {
      controller:'UserListController',
      templateUrl:'/dist/views/schools/users/all.html'
    })
    .when('/schools/:schoolID/galleries/create', {
      controller:'GalleryCreateController',
      templateUrl:'/dist/views/schools/galleries/create.html'
    })
    .when('/schools/:schoolID/galleries', {
      controller:'GalleryListController',
      templateUrl:'/dist/views/schools/galleries/all.html'
    })
    .when('/schools/:schoolID/galleries/:id/edit', {
      controller:'GalleryEditController',
      templateUrl:'/dist/views/schools/galleries/edit.html'
    })
    .when('/schools/:schoolID/galleries/:id', {
      controller:'GalleryViewController',
      templateUrl:'/dist/views/schools/galleries/view.html'
    })
    .when('/dashboard', {
        controller:'DashboardController',
        templateUrl:'/dist/views/dashboard.html'
    })
    .when('/', {
        controller:'HomeController',
        data: {
          requiredAuth: false
        }
    })
    .otherwise({
        redirectTo:'/'
    });
})
.controller('LoginController', function($scope, HttpService, $location,$rootScope,AuthService) {
    $scope.doLogin = function () {
        if($scope.username && $scope.password){
            HttpService.post("login", {
                username: $scope.username,
                password: $scope.password,
            }).then(function (data) {
                if(data.data.user){
                    AuthService.setAuthUser(data.data.user);
                    $location.path('/dashboard');
                } else {
                    alert("Invalid Email or Password");
                }
            });
        } else {
            alert("Please enter username & Password");
        }
    };
})
.controller('LogoutController', ['$location','HttpService','$rootScope',function ($location,HttpService,$rootScope) {
    HttpService.get('logout').then(function (data) {
        if(data.data.success){
            $location.path('/');
        }
    });
}])
.controller('SchoolSignupController', function($scope, $http) {
    $scope.doSignup = function () {
        $http.post("api/v1/schools/signup", {
            name: $scope.name,
            phone: $scope.phone,
            email: $scope.email,
            domain: $scope.domain,
            username: $scope.username,
            password: $scope.password,
            password_confirmation: $scope.confirm_password,
        }).then(function (data) {
            if(data.data.school){
                $location.path('/login');
            } else {
                alert("There was error while signup");
            }
        });
    };
})

.controller('InviteCreateController', function($scope, HttpService, $routeParams,$location) {
    HttpService.get("roles").then(function (data) {
        $scope.roles = data.data;
    });

    $scope.invite = function () {
        HttpService.post("schools/"+$routeParams.schoolID+"/invites", {
            name: $scope.name,
            email: $scope.email,
            phone: $scope.phone,
            role_id: $scope.role_id,
        }).then(function (data) {
            $location.path('schools/'+$routeParams.schoolID+'/invites');
        });
    };
})
.controller('InviteListController', function($scope, $http,HttpService,$routeParams,$location,_,$route) {
    $scope.invites = {};
    $scope.schoolID = $routeParams.schoolID;

    HttpService.get("roles").then(function (data) {
        $scope.roles = data.data;

        HttpService.get('schools/'+$routeParams.schoolID+'/invites').then(function (data) {
            var invites = [];
            angular.forEach(data.data.data, function (invite,index) {
                invites.push(angular.extend(invite, {role: _.find($scope.roles, {id: invite.role_id})}));
            });
            $scope.invites = angular.extend(data.data, {data: invites});
        });
    });

    $scope.delete = function (id) {
        HttpService.delete('schools/'+$routeParams.schoolID+'/invites/'+id).then(function () {
            $route.reload();
        });
    };

})
.controller('InviteEditController', function($scope, $http,HttpService,$routeParams,$location) {
    $scope.invite = {};

    HttpService.get("roles").then(function (data) {
        $scope.roles = data.data;
    });

    HttpService.get("schools/"+$routeParams.schoolID+"/invites/"+$routeParams.token).then(function (data) {
        $scope.invite = data.data;
    });

    $scope.update = function () {
        HttpService.put("schools/"+$routeParams.schoolID+"/invites/"+$routeParams.token, {
            name: $scope.invite.name,
            email: $scope.invite.email,
            phone: $scope.invite.phone,
            role_id: $scope.invite.role_id,
        }).then(function (data) {
            $location.path('schools/'+$routeParams.schoolID+'/invites');
        });
    };
})

.controller('NewsCreateController', function($scope, $http, HttpService, $routeParams, $location) {
    $scope.add = function () {
        HttpService.post("schools/"+$routeParams.schoolID+"/news", {
            title: $scope.title,
            description: $scope.description,
        }).then(function (data) {
            $location.path('schools/'+$routeParams.schoolID+'/news');
        });
    };
})
.controller('NewsListController', function($scope, $http, HttpService, $routeParams, $location,$route) {
    $scope.news = {};
    $scope.schoolID = $routeParams.schoolID;

    HttpService.get("schools/"+$routeParams.schoolID+"/news").then(function (data) {
        $scope.news = data.data;
    });

    $scope.delete = function (id) {
        HttpService.delete("schools/"+$routeParams.schoolID+"/news/"+id).then(function (data) {
            $route.reload();
        });
    };

})
.controller('NewsEditController', function($scope, $http, HttpService, $routeParams, $location) {
    $scope.news = {};

    HttpService.get('schools/'+$routeParams.schoolID+'/news/'+$routeParams.id).then(function(data){
        $scope.news = data.data;
    });

    $scope.update = function () {
        HttpService.put("schools/"+$routeParams.schoolID+"/news/"+$routeParams.id, {
            title: $scope.news.title,
            description: $scope.news.description
        }).then(function (data) {
            $location.path('schools/'+$routeParams.schoolID+'/news');
        });
    };
})
.controller('UserListController', function($scope, $http, HttpService, $routeParams, $location,$route,_) {
    $scope.users = {};
    $scope.schoolID = $routeParams.schoolID;

    HttpService.get("roles").then(function (data) {
        $scope.roles = data.data;

        HttpService.get("schools/"+$routeParams.schoolID+"/users",{with: "role"}).then(function (data) {
            var users = [];
            angular.forEach(data.data.data, function (user,index) {
                users.push(angular.extend(user, {role: _.find($scope.roles, {id: user.pivot.role_id})}));
            });
            $scope.users = angular.extend(data.data, {data: users});
        });
    });



    $scope.remove = function (id) {
        HttpService.delete("schools/"+$routeParams.schoolID+"/users/"+id).then(function (data) {
            $route.reload();
        });
    };
})

.controller('FeedbackCreateController', function($scope, $http, HttpService, $routeParams, $location) {
    $scope.add = function () {
        HttpService.post("schools/"+$routeParams.schoolID+"/feedbacks", {
            feedback: $scope.feedback,
        }).then(function (data) {
            $location.path('schools/'+$routeParams.schoolID+'/feedbacks');
        });
    };
})
.controller('FeedbackListController', function($scope, $http, HttpService, $routeParams, $location,$route) {
    $scope.feedbacks = {};
    $scope.schoolID = $routeParams.schoolID;

    HttpService.get("schools/"+$routeParams.schoolID+"/feedbacks",{with: "user"}).then(function (data) {
        $scope.feedbacks = data.data;
    });

    $scope.delete = function (id) {
        HttpService.delete("schools/"+$routeParams.schoolID+"/feedbacks/"+id).then(function (data) {
            $route.reload();
        });
    };

})
.controller('FeedbackEditController', function($scope, $http, HttpService, $routeParams, $location) {
    $scope.feedback = {};

    HttpService.get('schools/'+$routeParams.schoolID+'/feedbacks/'+$routeParams.id).then(function(data){
        $scope.feedback = data.data;
    });

    $scope.update = function () {
        HttpService.put("schools/"+$routeParams.schoolID+"/feedbacks/"+$routeParams.id, {
            feedback: $scope.feedback.feedback,
        }).then(function (data) {
            $location.path('schools/'+$routeParams.schoolID+'/feedbacks');
        });
    };
})

.controller('GalleryCreateController', function($scope, HttpService, $routeParams,$location) {
    $scope.create = function () {
        HttpService.post("schools/"+$routeParams.schoolID+"/galleries", {
            name: $scope.name,
            description: $scope.description
        }).then(function (data) {
            $location.path('schools/'+$routeParams.schoolID+'/galleries');
        });
    };

    $scope.cancel = function () {
        $location.path('schools/'+$routeParams.schoolID+'/galleries');
    };
})
.controller('GalleryListController', function($scope, $http,HttpService,$routeParams,$location,_,$route) {
    $scope.galleries = {};
    $scope.schoolID = $routeParams.schoolID;

    HttpService.get('schools/'+$routeParams.schoolID+'/galleries').then(function (data) {
        $scope.galleries = data.data;
    });

    $scope.delete = function (id) {
        HttpService.delete('schools/'+$routeParams.schoolID+'/galleries/'+id).then(function () {
            $route.reload();
        });
    };

})
.controller('GalleryEditController', function($scope, $http,HttpService,$routeParams,$location) {
    $scope.gallery = {};

    $scope.update = function () {
        HttpService.put("schools/"+$routeParams.schoolID+"/galleries/"+$scope.gallery.id, {
            name: $scope.gallery.name,
            description: $scope.gallery.description
        }).then(function (data) {
            $location.path('schools/'+$routeParams.schoolID+'/galleries');
        });
    };

    $scope.cancel = function () {
        $location.path('schools/'+$routeParams.schoolID+'/galleries');
    };

    HttpService.get("schools/"+$routeParams.schoolID+"/galleries/"+$routeParams.id).then(function (data) {
        $scope.gallery = data.data;
    });
})
.controller('GalleryViewController', function($scope, $http,HttpService,$routeParams,$location,$uibModal) {
    $scope.gallery = {};
    $scope.imageModal = null;
    $scope.uploader = {
        files: [1],
        uploadingImages: [],
        filesSelected: function (files) {
            console.log(files);
        }
    };

    HttpService.get("schools/"+$routeParams.schoolID+"/galleries/"+$routeParams.id, {
        with: ['images']
    }).then(function (data) {
        $scope.gallery = data.data;
    });

    $scope.back = function () {
        $location.path('schools/'+$routeParams.schoolID+'/galleries');
    };

    $scope.uploadImages = function () {
        $scope.imageModal = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'myModalContent.html',
            size: 'lg'
        });


        $scope.imageModal.result.then(function () {
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.imageUploaded = function () {
        console.log(arguments);
    };

    $scope.closeUploadImagesModel = function () {
        $scope.imageModal.close();
    };

    window.x = $scope;
})


.controller('HomeController', ['$location','AuthService',function ($location,AuthService) {
    if(!AuthService.getAuthUser()){
        $location.path('/dashboard');
    } else {
        $location.path('/login');
    }
}])
.controller('DashboardController', ['$location','HttpService','$scope','$rootScope',function ($location,HttpService,$scope,$rootScope) {
    $scope.schools = [];
    $rootScope.school = null;

    $scope.selectSchool = function (school) {
        $rootScope.school = school;

        $location.path('/schools/'+school.id);
    };

    HttpService.get('users/me/schools').then(function (data) {
        angular.forEach(data.data, function (school) {
            $scope.schools.push(school);
        });
    });
}])

.controller('SchoolDashboardController', ['$location','HttpService','$rootScope','$scope','$routeParams',function ($location,HttpService,$rootScope,$scope,$routeParams) {
    $scope.invites = {
        pending: 0,
        total: 0,
    };
    $scope.news = {
        total: 0,
    };
    $scope.feedbacks = {
        total: 0,
    };
    $scope.users = {
        total: 0,
    };

    HttpService.get('schools/'+$routeParams.schoolID+'/news').then(function (data) {
        $scope.news.total = data.data.total;
    });

    HttpService.get('schools/'+$routeParams.schoolID+'/invites').then(function (data) {
        $scope.invites.total = data.data.total;
    });

    HttpService.get('schools/'+$routeParams.schoolID+'/invites',{
        "where[]": "user_id=0"
    }).then(function (data) {
        $scope.invites.pending = data.data.total;
    });

    HttpService.get('schools/'+$routeParams.schoolID+'/feedbacks').then(function (data) {
        $scope.feedbacks.total = data.data.total;
    });

    HttpService.get('schools/'+$routeParams.schoolID+'/users').then(function (data) {
        $scope.users.total = data.data.total;
    });
}])
.controller('SchoolHeaderPartialController', ['$location','HttpService','$rootScope','$routeParams','$timeout',function ($location,HttpService,$rootScope,$routeParams,$timeout) {
    $timeout(function () {
        if(!$rootScope.school && $routeParams.schoolID){
            HttpService.get('schools/'+$routeParams.schoolID).then(function (data) {
                $rootScope.school = data.data;
            });
        }
    },200);
}])

// For Confirm Popup
app.directive('ngConfirmClick', [
    function(){
    return {
        link: function (scope, element, attr) {
            var msg = attr.ngConfirmClick || "Are you sure?";
            var clickAction = attr.confirmedClick;
            element.bind('click',function (event) {
                if ( window.confirm(msg) ) {
                    scope.$eval(clickAction)
                }
            });
        }
    };
}]);

app.factory('AuthService', ['HttpService','$q','$rootScope','$cookies', function (HttpService,$q,$rootScope,$cookies) {
    return {
        getAuthUser: function () {
            return $cookies.getObject('user');
        },
        setAuthUser: function (user) {
            var d = new Date();
            d.setMonth(d.getMonth() - 1);

            $rootScope.user = user;

            $cookies.putObject('user',user);
        }
    }
}])
.factory('HttpService', ['$http', '$q','$httpParamSerializer','$rootScope', function ($http,$q,$httpParamSerializer,$rootScope) {
    return {
        get: function (url,data) {
            if(!data){
                data = {};
            }
            data.api_token = $rootScope.user ? $rootScope.user.api_token : null;
            return $http.get("api/v1/"+url+"?"+$httpParamSerializer(data));
        },
        post: function (url,data) {
            if(!data){
                data = {};
            }
            data.api_token = $rootScope.user ? $rootScope.user.api_token : null;
            return $http.post("api/v1/"+url,data);
        },
        put: function (url,data) {
            if(!data){
                data = {};
            }
            data.api_token = $rootScope.user ? $rootScope.user.api_token : null;
            return $http.put("api/v1/"+url,data);
        },
        delete: function (url) {
            data = {api_token: $rootScope.user ? $rootScope.user.api_token : null};
            return $http.delete("api/v1/"+url+"?"+$httpParamSerializer(data));
        }
    }
}])
.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       scope: {
            onChange: '=',
            files: '='
       },
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          console.log(scope.files);
          element.bind('change', function(){
              var files = [];

              for (var i = 0; i < element[0].files.length; i++) {
                  files.push(element[0].files[i]);
              }

              scope.files = files;

              if(scope.onChange && typeof(scope.onChange) == "function"){
                  console.log(scope.onChange,scope.files);
                  scope.onChange(scope.files);
              }
          });
       }
    };
}]);

app.run(['$rootScope', '$location', 'AuthService', function ($rootScope, $location, AuthService) {
    $rootScope.user = AuthService.getAuthUser();

    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        // if(!next.data || next.data.requiredAuth){
        //     if(user === undefined){
        //         AuthService.getAuthUser().then(function (user) {
        //             if(!user){
        //                 $location.path('/login');
        //             }
        //         });
        //     } else if(!user) {
        //         event.preventDefault();
        //         $location.path('/login');
        //     }
        // }
    });
}])


window.x = app;
